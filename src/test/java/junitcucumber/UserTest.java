package junitcucumber;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions( format = {"json:target/cucumber.json"},
features= "src/test/resources",
glue = "UserSteps.java")

public class UserTest {

}
